'# Copyright (c) 2008-2011 Wesley Werner
'# Source code distributed under the BSD license

''' <summary>
''' Copy info structure passed between our asynchronous copy calls
''' </summary>
''' <remarks></remarks>
Public Class CopyInfo

    Private _DestinationFile As String
    Public Property DestinationFile() As String
        Get
            Return _DestinationFile
        End Get
        Set(ByVal value As String)
            _DestinationFile = value
        End Set
    End Property


    Private _Sourcefile As String
    Public Property Sourcefile() As String
        Get
            Return _Sourcefile
        End Get
        Set(ByVal value As String)
            _Sourcefile = value
        End Set
    End Property


    Private _Length As Long
    Public Property Length() As Long
        Get
            Return _Length
        End Get
        Set(ByVal value As Long)
            _Length = value
        End Set
    End Property

End Class
