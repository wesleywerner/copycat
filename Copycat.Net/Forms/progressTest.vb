'# Copyright (c) 2008-2011 Wesley Werner
'# Source code distributed under the BSD license
Public Class progressTest

    Private tot As Integer = 0
    Private rnd As New Random

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        ' filesize * 0.001
        ' 1% of filesize
        tot += CInt(GraphProgressBar1.Filesize * 0.001)

        If tot > GraphProgressBar1.Filesize Then
            GraphProgressBar1.Reset()
            tot = 1
        End If

        If rnd.Next(100) < 30 Then
            GraphProgressBar1.Setvalue(tot, True)
        Else
            GraphProgressBar1.Setvalue(tot, False)
        End If

    End Sub

    Private Sub progressTest_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GraphProgressBar1.Filesize = 30000000
    End Sub
End Class