Imports System.Windows.Forms

Public Class SettingsForm

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Try
            My.Settings.RetryCount = CInt(txtRetries.Value)
            My.Settings.Buffersize = CInt(txtBuffer.Value * 1000)
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            Err(ex)
        End Try
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnDefaults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefaults.Click
        txtRetries.Value = 0
        txtBuffer.Value = 1000
    End Sub

    Private Sub SettingsForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtBuffer.Value = CDec(My.Settings.Buffersize / 1000)
            txtRetries.Value = My.Settings.RetryCount
        Catch ex As Exception
            btnDefaults.PerformClick()
        End Try
    End Sub

End Class
