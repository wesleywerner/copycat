'# Copyright (c) 2008-2011 Wesley Werner
'# Source code distributed under the BSD license
Imports System.Windows.Forms

''' <summary>
''' Analyze the integrity of a file
''' </summary>
''' <remarks></remarks>
Public Class AnalyzeForm

    Private WithEvents copy As New ASyncCopy
    Private path As String = ""
    Private hasProblems As Boolean = False


    ''' <summary>
    ''' close button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        lblNotice.Visible = True
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


    ''' <summary>
    ''' form closing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub AnalyzeForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If copy.Running Then
            e.Cancel = True
            Me.Text = "Please wait while closing..."
            Cancel_Button.Enabled = False
            copy.Abort()
        End If

    End Sub


    ''' <summary>
    ''' form load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub AnalyzeForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Dim info As New IO.FileInfo(path)

            ' set labels
            lblFile.Text = IO.Path.GetFileName(path)
            lblPercentage.Text = "Calculating ..."

            ' set the buffer 10% of the filesize, so it reads only 10 times
            copy.BufferSize = CInt(info.Length * 0.1)

            ' toggle simulation mode
            copy.Simulation = True

            ' set the target form so async code works
            copy.ParentForm = Me

            ' start the copy
            copy.CopyFile(path, Nothing)

            ProgressBar1.Maximum = CInt(info.Length / 1000)

        Catch ex As Exception
            Err(ex)
        End Try

    End Sub


    ''' <summary>
    ''' new constructor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New(ByVal basepath As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        path = basepath

    End Sub


    ''' <summary>
    ''' abort copy
    ''' </summary>
    ''' <param name="filename"></param>
    ''' <remarks></remarks>
    Private Sub copy_CopyCancelled(ByVal filename As String) Handles copy.CopyCancelled

        Me.Text = "Analyze Cancelled"
        Cancel_Button.Enabled = True
        Cancel_Button.Text = "&Close"
        ProgressBar1.Value = 0

    End Sub


    ''' <summary>
    ''' copy done
    ''' </summary>
    ''' <param name="filename"></param>
    ''' <remarks></remarks>
    Private Sub copy_CopyDone(ByVal filename As String, ByVal length As Long) Handles copy.CopyDone

        Me.Text = "Analyzed Disc"
        Cancel_Button.Enabled = True
        Cancel_Button.Text = "&Close"
        ProgressBar1.Value = ProgressBar1.Maximum

        If hasProblems Then
            lblPercentage.ForeColor = Color.Red
        Else
            lblPercentage.ForeColor = Color.Blue
        End If

    End Sub

    Private Delegate Sub copy_CopyProgressCallback(ByVal position As Long, ByVal goodRead As Boolean, ByVal retryCount As Integer)

    Private Sub copy_CopyError(ByVal filename As String, ByVal ex As System.Exception) Handles copy.CopyError

        Me.Text = "Analyze Error"
        Cancel_Button.Enabled = True
        Cancel_Button.Text = "&Close"
        ProgressBar1.Value = 0
        Err(ex)

    End Sub


    ''' <summary>
    ''' copy progress
    ''' </summary>
    ''' <param name="position"></param>
    ''' <param name="goodRead"></param>
    ''' <param name="retryCount"></param>
    ''' <remarks></remarks>
    Private Sub copy_CopyProgress(ByVal position As Long, ByVal goodRead As Boolean, ByVal retryCount As Integer) Handles copy.CopyProgress

        If Me.InvokeRequired Then
            Me.Invoke(New copy_CopyProgressCallback(AddressOf copy_CopyProgress), position, goodRead, retryCount)
            Exit Sub
        End If

        Try

            Dim divisor As Long = copy.BytesFailed + copy.BytesSucceeded
            If divisor = 0 Then divisor = 1

            lblPercentage.Text = String.Format("{0} % readable", FormatNumber((copy.BytesSucceeded / divisor) * 100, 2))

            ProgressBar1.Value = CInt(copy.BytesSucceeded / 1000)

            If (Not goodRead) Then hasProblems = True

        Catch ex As Exception
            'Err(ex)
        End Try

    End Sub


End Class
