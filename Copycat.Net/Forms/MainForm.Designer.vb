<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.progressTimer = New System.Windows.Forms.Timer(Me.components)
        Me.txtLog = New System.Windows.Forms.TextBox
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblLog = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblPosition = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.OpenDiscTool = New System.Windows.Forms.ToolStripDropDownButton
        Me.AnalyzeTool = New System.Windows.Forms.ToolStripButton
        Me.CopyTool = New System.Windows.Forms.ToolStripButton
        Me.AboutTool = New System.Windows.Forms.ToolStripButton
        Me.StopTool = New System.Windows.Forms.ToolStripButton
        Me.SkipTool = New System.Windows.Forms.ToolStripButton
        Me.FileList = New System.Windows.Forms.ListView
        Me.fileCol = New System.Windows.Forms.ColumnHeader
        Me.pathCol = New System.Windows.Forms.ColumnHeader
        Me.sizeCol = New System.Windows.Forms.ColumnHeader
        Me.statusCol = New System.Windows.Forms.ColumnHeader
        Me.ctmListview = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CheckAllTool = New System.Windows.Forms.ToolStripMenuItem
        Me.CheckNoneTool = New System.Windows.Forms.ToolStripMenuItem
        Me.InvertChecksTool = New System.Windows.Forms.ToolStripMenuItem
        Me.AnalyzeThisFileTool = New System.Windows.Forms.ToolStripMenuItem
        Me.splitListLog = New System.Windows.Forms.SplitContainer
        Me.progressGraph = New Copycat.Net.GraphProgressBar
        Me.ctmOpenActions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.BrowseTool = New System.Windows.Forms.ToolStripMenuItem
        Me.SettingsTool = New System.Windows.Forms.ToolStripButton
        ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.StatusStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ctmListview.SuspendLayout()
        Me.splitListLog.Panel1.SuspendLayout()
        Me.splitListLog.Panel2.SuspendLayout()
        Me.splitListLog.SuspendLayout()
        Me.ctmOpenActions.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenuItem1
        '
        ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'progressTimer
        '
        Me.progressTimer.Interval = 2500
        '
        'txtLog
        '
        Me.txtLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLog.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLog.Location = New System.Drawing.Point(0, 0)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLog.Size = New System.Drawing.Size(150, 46)
        Me.txtLog.TabIndex = 2
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblLog, Me.lblPosition})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 492)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(709, 22)
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblLog
        '
        Me.lblLog.Name = "lblLog"
        Me.lblLog.Size = New System.Drawing.Size(694, 17)
        Me.lblLog.Spring = True
        Me.lblLog.Text = "Ready"
        Me.lblLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenDiscTool, Me.AnalyzeTool, Me.CopyTool, Me.AboutTool, Me.StopTool, Me.SkipTool, Me.SettingsTool})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(709, 25)
        Me.ToolStrip1.TabIndex = 6
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'OpenDiscTool
        '
        Me.OpenDiscTool.Image = Global.Copycat.Net.My.Resources.Resources.dvd
        Me.OpenDiscTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenDiscTool.Name = "OpenDiscTool"
        Me.OpenDiscTool.Size = New System.Drawing.Size(65, 22)
        Me.OpenDiscTool.Text = "Open"
        Me.OpenDiscTool.ToolTipText = "Open a disc or location to copy"
        '
        'AnalyzeTool
        '
        Me.AnalyzeTool.Image = CType(resources.GetObject("AnalyzeTool.Image"), System.Drawing.Image)
        Me.AnalyzeTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AnalyzeTool.Name = "AnalyzeTool"
        Me.AnalyzeTool.Size = New System.Drawing.Size(68, 22)
        Me.AnalyzeTool.Text = "Analyze"
        Me.AnalyzeTool.ToolTipText = "Analyze the selected file"
        '
        'CopyTool
        '
        Me.CopyTool.Image = CType(resources.GetObject("CopyTool.Image"), System.Drawing.Image)
        Me.CopyTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CopyTool.Name = "CopyTool"
        Me.CopyTool.Size = New System.Drawing.Size(55, 22)
        Me.CopyTool.Text = "Copy"
        Me.CopyTool.ToolTipText = "Copy all checked files"
        '
        'AboutTool
        '
        Me.AboutTool.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.AboutTool.Image = CType(resources.GetObject("AboutTool.Image"), System.Drawing.Image)
        Me.AboutTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AboutTool.Name = "AboutTool"
        Me.AboutTool.Size = New System.Drawing.Size(60, 22)
        Me.AboutTool.Text = "About"
        Me.AboutTool.ToolTipText = "About Copycat.Net"
        '
        'StopTool
        '
        Me.StopTool.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.StopTool.Image = CType(resources.GetObject("StopTool.Image"), System.Drawing.Image)
        Me.StopTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.StopTool.Name = "StopTool"
        Me.StopTool.Size = New System.Drawing.Size(51, 22)
        Me.StopTool.Text = "Stop"
        Me.StopTool.ToolTipText = "Stop copying"
        '
        'SkipTool
        '
        Me.SkipTool.Image = CType(resources.GetObject("SkipTool.Image"), System.Drawing.Image)
        Me.SkipTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SkipTool.Name = "SkipTool"
        Me.SkipTool.Size = New System.Drawing.Size(70, 22)
        Me.SkipTool.Text = "Skip File"
        Me.SkipTool.ToolTipText = "Skip the current file"
        '
        'FileList
        '
        Me.FileList.CheckBoxes = True
        Me.FileList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.fileCol, Me.pathCol, Me.sizeCol, Me.statusCol})
        Me.FileList.ContextMenuStrip = Me.ctmListview
        Me.FileList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FileList.FullRowSelect = True
        Me.FileList.Location = New System.Drawing.Point(0, 0)
        Me.FileList.Name = "FileList"
        Me.FileList.Size = New System.Drawing.Size(685, 412)
        Me.FileList.TabIndex = 7
        Me.FileList.UseCompatibleStateImageBehavior = False
        Me.FileList.View = System.Windows.Forms.View.Details
        '
        'fileCol
        '
        Me.fileCol.Text = "File"
        '
        'pathCol
        '
        Me.pathCol.Text = "Path"
        '
        'sizeCol
        '
        Me.sizeCol.Text = "Size"
        Me.sizeCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'statusCol
        '
        Me.statusCol.Text = "Status"
        '
        'ctmListview
        '
        Me.ctmListview.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckAllTool, Me.CheckNoneTool, Me.InvertChecksTool, ToolStripMenuItem1, Me.AnalyzeThisFileTool})
        Me.ctmListview.Name = "ctmListview"
        Me.ctmListview.Size = New System.Drawing.Size(169, 98)
        '
        'CheckAllTool
        '
        Me.CheckAllTool.Name = "CheckAllTool"
        Me.CheckAllTool.Size = New System.Drawing.Size(168, 22)
        Me.CheckAllTool.Text = "Check &All"
        '
        'CheckNoneTool
        '
        Me.CheckNoneTool.Name = "CheckNoneTool"
        Me.CheckNoneTool.Size = New System.Drawing.Size(168, 22)
        Me.CheckNoneTool.Text = "Check &None"
        '
        'InvertChecksTool
        '
        Me.InvertChecksTool.Name = "InvertChecksTool"
        Me.InvertChecksTool.Size = New System.Drawing.Size(168, 22)
        Me.InvertChecksTool.Text = "&Invert Checks"
        '
        'AnalyzeThisFileTool
        '
        Me.AnalyzeThisFileTool.Image = CType(resources.GetObject("AnalyzeThisFileTool.Image"), System.Drawing.Image)
        Me.AnalyzeThisFileTool.Name = "AnalyzeThisFileTool"
        Me.AnalyzeThisFileTool.Size = New System.Drawing.Size(168, 22)
        Me.AnalyzeThisFileTool.Text = "Analy&ze this file ..."
        '
        'splitListLog
        '
        Me.splitListLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.splitListLog.Location = New System.Drawing.Point(12, 28)
        Me.splitListLog.Name = "splitListLog"
        Me.splitListLog.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitListLog.Panel1
        '
        Me.splitListLog.Panel1.Controls.Add(Me.FileList)
        '
        'splitListLog.Panel2
        '
        Me.splitListLog.Panel2.Controls.Add(Me.txtLog)
        Me.splitListLog.Panel2Collapsed = True
        Me.splitListLog.Size = New System.Drawing.Size(685, 412)
        Me.splitListLog.SplitterDistance = 216
        Me.splitListLog.TabIndex = 8
        '
        'progressGraph
        '
        Me.progressGraph.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progressGraph.BackColor = System.Drawing.Color.White
        Me.progressGraph.BadColor = System.Drawing.Color.Black
        Me.progressGraph.Filesize = CType(100, Long)
        Me.progressGraph.GoodColor = System.Drawing.Color.LightGreen
        Me.progressGraph.Location = New System.Drawing.Point(12, 446)
        Me.progressGraph.Name = "progressGraph"
        Me.progressGraph.Resolution = 100
        Me.progressGraph.Size = New System.Drawing.Size(685, 43)
        Me.progressGraph.TabIndex = 0
        '
        'ctmOpenActions
        '
        Me.ctmOpenActions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BrowseTool})
        Me.ctmOpenActions.Name = "ContextMenuStrip1"
        Me.ctmOpenActions.Size = New System.Drawing.Size(125, 26)
        '
        'BrowseTool
        '
        Me.BrowseTool.Image = Global.Copycat.Net.My.Resources.Resources.open_file1
        Me.BrowseTool.Name = "BrowseTool"
        Me.BrowseTool.Size = New System.Drawing.Size(124, 22)
        Me.BrowseTool.Text = "&Browse ..."
        '
        'SettingsTool
        '
        Me.SettingsTool.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.SettingsTool.Image = CType(resources.GetObject("SettingsTool.Image"), System.Drawing.Image)
        Me.SettingsTool.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SettingsTool.Name = "SettingsTool"
        Me.SettingsTool.Size = New System.Drawing.Size(69, 22)
        Me.SettingsTool.Text = "Settings"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 514)
        Me.Controls.Add(Me.splitListLog)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.progressGraph)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Text = "Copycat.Net"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ctmListview.ResumeLayout(False)
        Me.splitListLog.Panel1.ResumeLayout(False)
        Me.splitListLog.Panel2.ResumeLayout(False)
        Me.splitListLog.Panel2.PerformLayout()
        Me.splitListLog.ResumeLayout(False)
        Me.ctmOpenActions.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents progressGraph As Copycat.Net.GraphProgressBar
    Friend WithEvents progressTimer As System.Windows.Forms.Timer
    Friend WithEvents txtLog As System.Windows.Forms.TextBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblLog As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblPosition As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AnalyzeTool As System.Windows.Forms.ToolStripButton
    Friend WithEvents CopyTool As System.Windows.Forms.ToolStripButton
    Friend WithEvents FileList As System.Windows.Forms.ListView
    Friend WithEvents fileCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents pathCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents statusCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents splitListLog As System.Windows.Forms.SplitContainer
    Friend WithEvents StopTool As System.Windows.Forms.ToolStripButton
    Friend WithEvents SkipTool As System.Windows.Forms.ToolStripButton
    Friend WithEvents sizeCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents ctmListview As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CheckAllTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNoneTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvertChecksTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnalyzeThisFileTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutTool As System.Windows.Forms.ToolStripButton
    Friend WithEvents OpenDiscTool As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ctmOpenActions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents BrowseTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsTool As System.Windows.Forms.ToolStripButton
End Class
