<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class progressTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.GraphProgressBar1 = New Copycat.Net.GraphProgressBar
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'GraphProgressBar1
        '
        Me.GraphProgressBar1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GraphProgressBar1.BackColor = System.Drawing.Color.White
        Me.GraphProgressBar1.BadColor = System.Drawing.Color.Black
        Me.GraphProgressBar1.Filesize = CType(100, Long)
        Me.GraphProgressBar1.GoodColor = System.Drawing.Color.LightGreen
        Me.GraphProgressBar1.Location = New System.Drawing.Point(20, 26)
        Me.GraphProgressBar1.Name = "GraphProgressBar1"
        Me.GraphProgressBar1.Size = New System.Drawing.Size(250, 23)
        Me.GraphProgressBar1.TabIndex = 0
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 20
        '
        'progressTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(283, 72)
        Me.Controls.Add(Me.GraphProgressBar1)
        Me.Name = "progressTest"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "progressTest"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GraphProgressBar1 As Copycat.Net.GraphProgressBar
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
