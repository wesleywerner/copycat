'# Copyright (c) 2008-2011 Wesley Werner
'# Source code distributed under the BSD license

Copycat copies data off media that has read errors.
Usually when media has read errors, the process accessing the data aborts it's attempts. Copycat bypasses this abort signal, and keeps reading the data even during read errors. 
It may not recover all damaged files completely, but it tries it's best.

Copycat can't guess or replace the missing chunks of data for you, there is no magic involved, and the data will have missing chunks where the media could not be read.

It is still usefull to copy data in some cases, for example - Video DVD's:
I had scratched DVD's that would not play, copying the video (.vob) files off did not work, as the copy failed at the first read error.
Copycat can read the files, while skipping the read errors.
This works because I can re-encode the video files to rebuild the stream data, with missing chunks introducing jumps in the video. Your milage may vary.

Copycat also works well for data discs that have a bunch of archived applications or documents.

For news and feedback, please visit Copycat's site at http://code.google.com/p/copycat/