'# Copyright (c) 2008-2011 Wesley Werner
'# Source code distributed under the BSD license
Module ErrorHandling

    Friend Sub Err(ByVal ex As Exception)

        MessageBox.Show(ex.ToString, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1, 0)

    End Sub

End Module
